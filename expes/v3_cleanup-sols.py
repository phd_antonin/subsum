import os
import shutil
import pandas as pd
import numpy as np
import uuid
import ast

def clean_df(df, expe):
    expe = expe.split("-")
    
    
    df["success"] = (df["nsols"] < df["pool_size"]) & df["wall_hit"] == False
    df["pool_size"] = int(expe[-1][1:]) 
    
    return df

def extract_sols(df, path_sols):
    uid = uuid.uuid4().hex[:10]
    df["uuid"] = uid
    
    sols = df["sols"]
    
    sols = ast.literal_eval(sols)
    
    sols = [[int(x) for x in sol] for sol in sols]
    
    if len(sols) == 0:
        sols = [None]
    
    vsols = np.vstack(sols)
    
    path_sol = os.path.join(path_sols, uid + ".npy")

    with open(path_sol, "wb") as f:
        np.save(f, vsols)
    
    del df["sols"]
    
    return df
    

base = "./v3/"

df = None

dirs = os.listdir(base)

for i, elem in enumerate(dirs):
    expe = os.path.join(base, elem)
    
    if os.path.isdir(expe):
        print(i + 1, "/", len(dirs), expe)
        
        expe_execo = os.path.join(expe, ".execo")
        expe_results = os.path.join(expe, "results")
        expe_sols = os.path.join(expe, "sols")
        expe_all = os.path.join(expe, "res_all.csv")
        
        shutil.rmtree(expe_execo, ignore_errors=True)
        
        if not os.path.exists(expe_sols):
            os.makedirs(expe_sols)
        
        df_expe = None
        
        for res in os.listdir(expe_results):
            res = os.path.join(expe_results, res)
            print("\t", res)
            
            df_tmp = pd.read_csv(res)
            
            df_tmp = clean_df(df_tmp, elem)
            df_tmp = df_tmp.apply(lambda x: extract_sols(x, expe_sols), axis=1)
            
            df_tmp.to_csv(res)
            
            if df_expe is None:
                df_expe = df_tmp
            else:
                df_expe = df_expe.append(df_tmp)
        
        
        df_expe.to_csv(expe_all)
        
        if df is None:
            df = df_expe
        else:
            df = df.append(df_expe)

df.to_csv("./v3_all.csv")