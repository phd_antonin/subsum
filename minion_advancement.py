import json
import os
import sys

if len(sys.argv) >= 2:
    path = os.path.abspath(sys.argv[1])

    path_queue = os.path.join(path, "queue.json")
    path_results = os.path.join(path, "results")

    total = len(json.loads(open(path_queue, "r").read()))
    done = 0

    for rpath in os.listdir(path_results):
        rpath = os.path.join(path_results, rpath)

        n = len(open(rpath, "r").read().split("\n"))
        done += n -1

    print("Total :", total)
    print("Done :", done)
    print(done * 100 / total, "%")


else:
    print("Please, set the experiment parameters directory in the first argument.")
    exit(-1)