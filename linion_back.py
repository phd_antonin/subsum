import json
import os
import sys
import traceback
import uuid
import random
from time import sleep

import minionize
from execo_engine import ParamSweeper, sweep
from minionize import FuncCallback, Param, Engine

from src.expe import single_expe
from src.minionsweep import explore_days, explore_days_hi, agg_size_bins, explore_days_bins, msweep

config = {
    "clients": [
        {
            "name": "30_base",
            "path": "./csv/30_base.csv",
            "subset": "first-100",
            "subset_size": 100,
            "param": None,
        }
    ],
    "noise": {
        'name': 'classic',
        'method': 'classic',
        'aggregate': 'avg',
        'param': None
    },
    #"noise": {
    #    'name': 'lpa-90-1-1',
    #    'method': 'lpa',
    #    'aggregate': 'sum',
    #    'param': {
    #       "outliers": 0.90,
    #       "epsilon": 1,
    #       "bound": 1
    #    }
    #},
    #"noise": {
    #    "name": "fpa-09-10-0.6-20",
    #    "method": "fpa",
    #    'aggregate': 'sum',
    #    "param": {
    #        "outliers": 0.90,
    #        "epsilon": 10,
    #        "k": 20,
    #        "bound": 0.3,
    #    }
    #},
    "agg_size": agg_size_bins(100, 10),  #[10],#list(range(1, 100, 10)),
    "methods": [{"name": "rng", "param": x} for x in range(1)],
    "days": explore_days_bins(100, 10),
    #"days": explore_days(2250, 4500, 450),
    #"days": explore_days_hi(1, 300, 10, 300, 16000, 500),
    "threads": None,
    "wall_time": 1000,
    "pooled": True,
    "pool_size": 100
}

def rnsleep():
    s = random.randint(1, 100) / 10
    sleep(s)


class MyFuncClb(FuncCallback):

    def __init__(self, expe_name, path_results, path_sols):

        if "OAR_JOB_ID" in os.environ:  # In OAR, uuid is the job id
            id = os.environ["OAR_JOB_ID"]
        else:  # Not in OAR, generate custom uuid
            id = uuid.uuid4().hex[:5]

        self.path_out = os.path.join(path_results, id + ".csv")
        self.path_sols = path_sols
        self.expe_name = expe_name

    def process(self, param: Param, engine: Engine):
        return single_expe(
            self.expe_name,
            param['clients_path'],
            param['clients_name'],
            param['clients_subset'],
            param['clients_param'],
            param['clients_size'],
            param['agg_size'],
            param['method_name'],
            param['method_param'],
            param['days_name'],
            param['days_param'],
            param['noise_name'],
            param['noise_method'],
            param['noise_param'],
            param['noise_aggregate'],
            param['threads'],
            param['wall_time'],
            param['pooled'],
            param['pool_size'],
            self.path_out,
            self.path_sols
        )


if __name__ == "__main__":
    rnsleep()

    # Build the parameters
    if len(sys.argv) >= 2:
        expe_name = sys.argv[1]
        path = os.path.abspath(expe_name)

        # Generate the paths
        path_json = os.path.join(path, "expe.json")
        path_queue = os.path.join(path, "queue.json")
        path_execo = os.path.join(path, ".execo")
        path_results = os.path.join(path, "results")
        path_sols = os.path.join(path, "sols")

        # Set the environment variables
        os.environ['MINION_ENGINE'] = "execo"
        os.environ['EXECO_PERSISTENCE_DIR'] = path_execo

        try:
            os.makedirs(path)

            # Create the result path
            if not os.path.exists(path_results):
                os.makedirs(path_results)

            # Create the solutions path
            if not os.path.exists(path_sols):
                os.makedirs(path_sols)

            # Write the experiments data parameters in json file
            open(path_json, "w").write(json.dumps(config, indent=2))

            # Create the execo queue from the data (json like) object and save it to
            sd = msweep(config)
            open(path_queue, "w").write(json.dumps(sd, indent=2))

            ParamSweeper(path_execo, sweeps=sd, save_sweeps=True)
        except FileExistsError as e:  # The directory already exist, so other thread is already creating everything.
            sleep(1)
        except Exception as e:  # Something else went wrong. It's not good
            traceback.print_exc()
            exit(-1)
    else:
        print("Please, set the experiment parameters directory in the first argument.")
        exit(-1)

    # Run the experiments
    m = minionize.minionizer(MyFuncClb(expe_name, path_results, path_sols))
    m.run()
