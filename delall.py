import subprocess

result = subprocess.run(['oarstat', '-u'], stdout=subprocess.PIPE)

for line in result.stdout.decode("utf-8").split("\n"):
    id = line.split(" ")[0]

    result = subprocess.run(['oardel', id], stdout=subprocess.PIPE)
    print(result.stdout.decode('utf-8'))