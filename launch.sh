#!/bin/bash
#OAR -l {mem_core > 10 * 1024}/cpu=1/core=2,walltime=144:00:00
#OAR -t besteffort
#OAR -t idempotent

#OAR -O /srv/tempdd/avoyez/ksepy/logs/%jobid%.output
#OAR -E /srv/tempdd/avoyez/ksepy/logs/%jobid%.error

. /etc/profile.d/modules.sh
set -eux

module load spack/singularity #singularity-3.2.1-gcc-4.9.2

pushd /srv/tempdd/avoyez/ksepy

singularity exec --bind /soft:/soft ./ksepy.sif python3 minion.py $1