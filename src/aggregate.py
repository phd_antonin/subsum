import math

import numpy as np
from numpy import ndarray
from numpy.linalg import linalg
import random

def norm(n: int, δ: float, order: int) -> float:
    serie = np.full((n,), δ)
    return linalg.norm(serie, order)


def bound(maximum) -> float:
    return 10 * math.ceil(maximum / 10)


def lpa(Q: ndarray, delta: float, epsilon: float = 0.1) -> ndarray:
    λ = delta / epsilon

    #print(Q.size)
    #print(λ)

    # Laplace mechanism applied to whole serie
    Z = np.random.laplace(scale=λ, size=Q.size)
    return Q + Z


def fpa(Q: ndarray, delta: float, epsilon: float = 0.1, k: int = 20) -> ndarray:
    F = np.fft.fft(Q)
    # first k values of DFT
    F_k = F[:k]

    # lpa of F_k
    Fλ_k = lpa(F_k, delta, epsilon)

    # Fλ_k with 'n - k' zero-padding
    Fλ_n = np.pad(Fλ_k, (0, Q.size - k))

    # inversse of trasform as final result
    return np.fft.ifft(Fλ_n)


def add_lpa_noise(df_clients, df_subset, outliers, epsilon, aggregate):
    df_subset = df_subset.filter_outliers(outliers)

    df_agg = df_subset.aggregate(aggregate)
    maximum = df_subset.get_max()

    boundary = bound(maximum)
    print("BB", boundary)

    sensitivity_lpa = norm(
        len(df_agg.df['agg']),
        boundary,
        1,
    )

    lbda = sensitivity_lpa / epsilon

    df_agg.df['clear'] = df_agg.df['agg']
    df_agg.df['ukn_n'] = 1

    df_agg.df['noise'] = np.random.laplace(scale=lbda, size=len(df_agg.df))
    df_agg.df['noise'] = df_agg.df['noise'].astype(int)

    df_agg.df['agg'] += df_agg.df['noise']

    # df_agg.df['ukn_max'] = 3 * np.random.laplace(scale=lbda, size=len(df_agg.df))
    # df_agg.df['ukn_max'] = int(df_agg.df['ukn_max'].max())
    # df_agg.df['ukn_min'] = - df_agg.df["ukn_max"]

    return df_clients, df_subset, df_agg


def add_fpa_noise(df_clients, df_subset, outliers, epsilon, k, setmax, aggregate):
    df_subset = df_subset.filter_outliers(outliers)

    df_agg = df_subset.aggregate(aggregate)
    maximum = df_subset.get_max()

    boundary = bound(maximum)
    #print("BB", boundary)
    fpa_sensitivity = math.sqrt(k) * norm(len(df_agg.df["agg"]), boundary, 2)

    df_agg.df['clear'] = df_agg.df['agg']
    df_agg.df['ukn_n'] = 1

    df_agg.df['agg'] = fpa(df_agg.df['agg'], fpa_sensitivity, epsilon, k)
    df_agg.df['agg'] = df_agg.df['agg'].astype(int)

    df_agg.df['noise'] = df_agg.df['agg'] - df_agg.df['clear']

    df_agg.df['ukn_max'] = int(df_agg.df["clear"].max() * setmax)
    df_agg.df['ukn_min'] = - df_agg.df['ukn_max']

    return df_clients, df_subset, df_agg


def unknown_aggregates(df_clients, df_subset, ds_pc, agg_pc, aggregate):
    df_agg = df_subset.aggregate(aggregate)

    ds_len = len(df_clients.df.columns[1:])
    agg_len = len(df_subset.df.columns[1:])

    ds_n = ds_len * ds_pc
    agg_n = agg_len * agg_pc

    agg_pos = set(df_subset.df.columns[1:])

    filtered = []

    while len(filtered) < agg_n and len(agg_pos) > 0:
        choice = random.choice(list(agg_pos))
        agg_pos.remove(choice)
        filtered.append(choice)

    ds_pos = set(df_clients.df.columns[1:]) - set(df_subset.df.columns[1:])

    while len(filtered) < ds_n and len(ds_pos) > 0:
        client = random.choice(list(ds_pos))
        ds_pos.remove(client)

        filtered.append(client)

    # Bound the noise
    n_removed = agg_len - agg_n
    maximum = df_subset.get_max()

    df_agg.df['ukn_n'] = 1
    df_agg.df['ukn_max'] = maximum * n_removed
    df_agg.df['ukn_min'] = - df_agg.df['ukn_max']

    df_clients = df_clients.get_subset_array(filtered)

    return df_clients, df_subset, df_agg


def build_aggregate(df_clients, df_subset, method, aggregate, params=None):
    if method == "classic":
        return df_clients, df_subset, df_subset.aggregate(aggregate)

    if method == "lpa":
        outliers = params["outliers"]
        epsilon = params['epsilon']
        bound = params['bound']

        return add_lpa_noise(df_clients, df_subset, outliers, epsilon, aggregate)

    if method == 'fpa':
        outliers = params["outliers"]
        epsilon = params['epsilon']
        k = params['k']
        bound = params['bound']

        return add_fpa_noise(df_clients, df_subset, outliers, epsilon, k, bound, aggregate)

    if method == 'ukn':
        ds_pc = params['ds_pc']
        agg_pc = params['agg_pc']

        return unknown_aggregates(df_clients, df_subset, ds_pc, agg_pc, aggregate)
