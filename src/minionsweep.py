import json
from math import floor
from execo_engine import HashableDict

def explore_days(start, end, step):
    exp = []

    for i in range(start, end, step):
        name = "num-start-" + str(i)
        exp += [{
            "name": name,
            "param": i,
        }]
    return exp


def explore_days_hi(ls, le, lr, ms, me, mr):
    exp = []

    for i in range(ls, le, lr):
        name = "num-start-" + str(i)
        exp += [{
            "name": name,
            "param": i
        }]

    for i in range(ms, me, mr):
        name = "num-start-" + str(i)
        exp += [{
            "name": name,
            "param": i
        }]

    return exp


def generate_bins(size, nbins):
    bins = []
    step = floor(size / nbins)

    for i in range(nbins):
        bins.append((i + 1) * step)

    return bins


def agg_size_bins(nclients, nbins):
    """
    Generate the aggregates size to explore (up to nclients / 2) with a given number of bins.
    """
    return generate_bins(floor(nclients / 2), nbins)


def explore_days_bins(ndays, nbins):
    """
    Generate the number of days to explore based on a total number of days
    """
    exp = []
    step = floor(ndays / nbins)

    for i in range(nbins):
        val = (i + 1) * step

        exp += [{
            "name": "num-start-" + str(val),
            "param": val
        }]

    return exp

def to_hashable_dict(d) -> HashableDict:
    h = HashableDict()

    if isinstance(d, list):
        return [to_hashable_dict(v) for v in d]

    for k, v in d.items():
        if isinstance(v, dict):
            h[k] = to_hashable_dict(v)
        else:
            h[k] = v
    return h


def _enum_build(config, client, agg_size, method, day):

    return [{
        "clients_name": client["name"],
        "clients_path": client["path"],
        "clients_subset": client["subset"],
        "clients_param": client["param"],
        "clients_size": client["subset_size"],
        "noise_name": config['noise']['name'],
        'noise_method': config['noise']['method'],
        'noise_param': config['noise']['param'],
        'noise_aggregate': config['noise']['aggregate'],
        "agg_size": agg_size,
        "method_name": method["name"],
        "method_param": method["param"],
        "days_name": day["name"],
        "days_param": day["param"],
        "threads": config["threads"],
        "wall_time": config["wall_time"],
        "pooled": config["pooled"],
        "pool_size": config["pool_size"]
    }]


def _enum_days(config, client, agg_size, method):
    results = []
    days = config["days"]

    for day in days:
        l = _enum_build(config, client, agg_size, method, day)
        results += l

    return results


def _enum_methods(config, client, agg_size):
    results = []
    methods = config["methods"]

    for method in methods:
        l = _enum_days(config, client, agg_size, method)
        results += l

    return results


def _enum_agg_size(config, client):
    results = []
    agg_sizes = config["agg_size"]

    for agg_size in agg_sizes:
        l = _enum_methods(config, client, agg_size)
        results += l

    return results


def _enum_clients(config):
    results = []
    clients = config["clients"]

    for client in clients:
        l = _enum_agg_size(config, client)
        results += l

    return results


def msweep(config):
    sd = _enum_clients(config)
    return to_hashable_dict(sd)


if __name__ == "__main__":
    config = {
        "clients": [
            {
                "name": "daily_base_150",
                "path": "./csv/daily_base.csv",
                "subset": "all",
                "param": None
            }
        ],
        "agg_size": list(range(1, 10)),
        "methods": [
            {
                "name": "rng-0",
                "param": 0
            }
        ],
        "days": explore_days(1, 5, 1),
        "threads": None,
        "wall_time": None,
        "pooled": True
    }

    l = msweep(config)
    print(json.dumps(l, indent=2))
