import numpy as np

def to_vector(solution, clients):
    """
    Transform a solution into a vector of {0,1}.
    """
    vector = []
    for c in clients:
        if c in solution:
            vector.append(1)
        else:
            vector.append(0)

    return np.array(vector)


def dist(solution, df_subset, df_clients):
    sol_vec = to_vector(solution, df_clients.df.columns[1:])
    agg_vec = to_vector(df_subset.df.columns[1:], df_clients.df.columns[1:])
    clients_len = len(df_clients.df.columns) - 2

    d = np.linalg.norm(sol_vec - agg_vec)
    b = np.linalg.norm(np.array([1] * clients_len))

    suc = 1 - d / b

    return suc


def is_all_part(solution, df):
    """
    Check if the solutions is made of clients if the given dataset
    """
    for c in solution:
        if c not in df.df.columns:
            return False
    return True


def is_valid_sum(df_solution, df_agg, days):
    """
    Check if the aggregates computed with the clients in the solution match the real aggregate on the given days
    """
    df_sol_agg = df_solution.aggregate()

    for day in days:
        sol = int(df_sol_agg.get_day(day)["agg"])
        agg = int(df_agg.get_day(day)['agg'])

        if sol != agg:
            return False
    return True


def check_solution(solution, df_clients, df_subset, df_agg, days):
    """
    Compute varieties of check of the solutions found by gurobi
    """
    score = dist(solution, df_subset, df_clients)

    df_solution = df_clients.get_subset_array(solution)
    all_clients = is_all_part(solution, df_clients)
    all_subset = is_all_part(solution, df_subset)

    sol_is_subset = set(solution) == set(df_subset.df.columns[1:])

    valid_size = len(solution) == len(df_subset.df.columns[1:])
    valid_sums = is_valid_sum(df_solution, df_agg, days)

    # print("Are the solutions clients :", all_clients)
    # print("Are the solutions in the subset :", all_subset)
    # print("Is the solutions size the same of the subset :", valid_size)
    # print("Is all the sums valid :", valid_sums)
    # print("The solutions is the subset :", sol_is_subset)

    return all_clients & all_subset & valid_size & valid_sums & sol_is_subset, valid_sums, score


def check_solutions(solutions, df_clients, df_subset, df_agg, days):
    valid_sums = False
    valid = False

    score_min = None
    score_max = None
    score_avg = 0

    for solution in solutions:
        valid, valid_sums, score = check_solution(solution, df_clients, df_subset, df_agg, days)

        if score_min is None:
            score_min = score
        if score_max is None:
            score_max = score

        if score < score_min:
            score_min = score
        if score > score_max:
            score_max = score

        score_avg += score

        if valid:
            valid = True

    if len(solutions) > 0:
        score_avg = score_avg / len(solutions)

    return valid, valid_sums, score_avg, score_min, score_max